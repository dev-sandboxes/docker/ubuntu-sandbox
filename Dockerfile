ARG IMAGE
ARG TAG
FROM ${IMAGE}:${TAG}

EXPOSE 80/tcp
EXPOSE 443/tcp

ARG USER
RUN apt-get update && \
    apt-get -y install \
      zsh \
    && \
    apt-get -y clean && \
    apt-get -y autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    useradd -ms /bin/bash ${USER}
USER ${USER}

RUN curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh || true && \
    echo 'export TERM="xterm-256color"' >> ~/.zshrc && \
    echo 'export TERM="xterm-256color"' >> ~/.bashrc

COPY ./home/ /home/${USER}
WORKDIR /home/${USER}

CMD [ "zsh" ]